from django.shortcuts import render
from bokeh.plotting import figure
from bokeh.embed import components
# from bokeh.models import HoverTool, LassoSelectTool, WheelZoomTool, PointDrawTool, ColumnDataSource
# from bokeh.palettes import Category20c, Spectral6
# from bokeh.transform import cumsum
# Create your views here.

from .models import Element

def starter(request):
	plot = figure()

	# вариант 1	
	xx = []
	yy = []
	for element in Element.objects.all():
		xx.append(element.atomic_number)
		yy.append(len(element.full_name))

	# вариант 2
	# xx = [element.atomic_number for element in Element.objects.all()]
	# yy = [len(element.full_name) for element in Element.objects.all()]
	plot.circle(xx, yy, size=20, color='red')

	script, div = components(plot)

	return render(
		request,
		'chemistry/starter.html',
		{
			'script': script,
			'div': div,
		}
	)
