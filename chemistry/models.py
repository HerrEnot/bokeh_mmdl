from django.db import models

# Create your models here.
class Element(models.Model):
	atomic_number = models.IntegerField('Атомный номер')
	short_name = models.CharField(max_length=4)
	full_name = models.CharField(max_length=15)

	def __str__(self):
	        return (str(self.short_name) + ' [' + str(self.atomic_number) + ']')